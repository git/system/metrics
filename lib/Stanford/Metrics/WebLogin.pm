# Stanford::Metrics::WebLogin -- Metrics reporting for WebLogin logs.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2010, 2013
#     The Board of Trustees of the Leland Stanford Junior University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Parses and reports on WebLogin logs.  The collected metrics are rates over
# time stored for:
#
#     Forced logins with password
#     Logins with password
#     Authentications using existing credentials
#     Total authentications
#     Authentications with OTP
#
# We only pay attention to lines like:
#
#     [Sun Mar 28 23:45:40 2010] [notice] mod_webkdc: event=requestToken
#     from=127.0.0.1 clientIp=128.12.74.243
#     server=krb5:webauth/axessauth.stanford.edu@stanford.edu user=cgoeders
#     rtt=id sa=webkdc login=password ro=fa lec=0
#
# The presence of lec= indicates whether or not the user had to authenticate
# with a password.  login=password indicates a normal password authentication;
# it may be login=otp for an OTP authentication.  ro=fa indicates a forced
# password authentication.  We skip any line that contains errorMessage or lem
# keys, since those indicate a failed login.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::WebLogin;

require 5.005;

use DB_File ();
use POSIX qw(strftime);
use Stanford::Metrics::Apache ();
use Stanford::Metrics::Report ();
use Stanford::Metrics::RRD ();

use strict;
use vars qw(@FIELDS @ISA $OTPUSERDB $OTPFACTORDB $OTPSERVERDB $OTPREQUESTDB
    $SERVERDB $SPEC $USERDB $VERSION %FACTORS %REQUESTS);

@ISA = qw(Stanford::Metrics::Apache);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.03';

# The fields stored in the RRD file for Kerberos information.
@FIELDS = qw(forced login auth total otp);

# The various factors and what we'd like to print them as.
%FACTORS = (o1 => 'List',
            o2 => 'SMS',
            o3 => 'Authenticator',
            o4 => 'Hardware',
           );

# The various requested factors and what we'd like to print them as.
%REQUESTS = (s_m  => 'Session Multifactor',
             s_o  => 'Session OTP',
             s_p  => 'Session Password',
             s_rm => 'Session Random',
             i_d  => 'Initial Device',
             i_m  => 'Initial Multifactor',
             i_o  => 'Initial OTP',
             i_p  => 'Initial Password',
             i_rm => 'Initial Random',
            );

# The paths to the Berkeley DB database used to store unique users, servers,
# and users using OTP.  The current year and month will be appended, followed
# by ".db".
$USERDB       = './data/weblogin/users';
$OTPUSERDB    = './data/weblogin/otpusers';
$OTPFACTORDB  = './data/weblogin/otpfactors';
$OTPREQUESTDB = './data/weblogin/otprequests';
$OTPSERVERDB  = './data/weblogin/otpservers';
$SERVERDB     = './data/weblogin/servers';

# The specification for the reports.
$SPEC = {
  name    => 'weblogin',
  rrd     => [ 'weblogin' ],
  sources => [ @FIELDS ],
  values  => [ [ total    => 'total' ] ],
  tables  => [ { headings => [ 'Type', 'Count', 'Percent' ],
                 data     => [ [ [ string  => 'Forced Password' ],
                                 [ total   => 'forced'          ],
                                 [ percent => 'forced', 'total' ] ],
                               [ [ string  => 'OTP Logins'      ],
                                 [ total   => 'otp'             ],
                                 [ percent => 'otp', 'total'    ] ],
                               [ [ string  => 'Password Logins' ],
                                 [ total   => 'login'           ],
                                 [ percent => 'login', 'total'  ] ],
                               [ [ string  => 'Single Sign-On'  ],
                                 [ total   => 'auth'            ],
                                 [ percent => 'auth', 'total'   ] ] ],
                 total    => [ [ string => 'TOTAL:' ],
                               [ total  => 'total'  ],
                               [ string => ''       ] ] } ],
  graphs  => [ { title    => 'Authentications per Minute',
                 file     => 'auth',
                 data     => [ 'forced', 'login', 'auth', 'otp' ],
                 commands => [ 'CDEF:forcedSC=forced,60,*',
                               'CDEF:loginSC=login,60,*',
                               'CDEF:authSC=auth,60,*',
                               'CDEF:otpSC=auth,60,*',
                               'AREA:forcedSC#CE3E00:Forced Login',
                               'AREA:loginSC#0028D2:Password:STACK',
                               'AREA:authSC#008800:Single Sign-On:STACK',
                               'AREA:otpSC#888888:OTP:STACK',
                           ] } ]
};

##############################################################################
# Log parsing
##############################################################################

# The user-visible parse interface.  Takes the root directory and a regex to
# match machines for the logs to parse and an optional path to the root of the
# RRD data.  Creates the RRD object that will store the metrics data and
# initializes the data storage in the object, and then hands things off to
# parse_logs.
sub parse {
    my ($self, $root, $pattern, $rrdroot) = @_;
    my %logs = $self->find_logs ($root, $pattern, 'error(?:_log)?');
    for my $system (sort keys %logs) {
        my $path = "weblogin/$system.rrd";
        $path = "$rrdroot/$path" if defined $rrdroot;
        $self->{RRD} = Stanford::Metrics::RRD->new ($path);
        unless ($self->{RRD}) {
            $self->{RRD} =
                Stanford::Metrics::RRD->create ($path, 'default', @FIELDS);
        }
        $self->{DATA} = [ (0) x scalar (@FIELDS) ];
        $self->{USERS} = {};
        $self->{OTPUSERS} = {};
        $self->{OTPSERVERS} = {};
        $self->{OTPFACTORS} = {};
        $self->{OTPREQUESTS} = {};
        $self->{SERVERS} = {};
        undef $self->{LAST};
        $self->parse_logs (@{ $logs{$system} });
    }
}

# Parse a single log line that's been broken down into timestamp and remainder
# of the line, and enter that data in the DATA array.
sub parse_data {
    my ($self, $time, $data) = @_;
    $data =~ s/^\[\S+\]\s+(?:\[client \S+\]\s+)?//;
    my ($server, $user)
        = ($data =~ /^mod_webkdc:\ event=requestToken\ from=\S+
                     \ clientIp=\S+\ server=(\S+)(?:\ url=\S+)?\ user=(\S+)/x);
    return unless $user;
    return if $data =~ / errorMessage=/;
    return if $data =~ / lem=/;

    $server =~ s{\@.*}{};
    $server =~ s{^krb5:webauth/}{};

    if ($data =~ / ro=(?:lc,)?fa[, ]/) {
        $self->{DATA}[0]++;
    } elsif ($data =~ / login=password/) {
        $self->{DATA}[1]++;
    } elsif ($data =~ / login=otp/) {
        my ($factors) = ($data =~ / ifactors=(\S+) /);
        $self->{DATA}[4]++;
        $self->{OTPUSERS}{$user}++;
        for my $factor (split (/,/, $factors)) {
            next unless $factor =~ /^o\d$/;
            $self->{OTPFACTORS}{$factor}++;
        }
    } else {
        $self->{DATA}[2]++;
    }

    # Check for requested factors in any successful login, along with the
    # requesting server.
    if ($data =~ / lec=0/) {
        if ($data =~ / w(i|s)factors=(\S+)/) {
            my ($type, $factors) = ($1, $2);
            for my $factor (split (/,/, $factors)) {
                $factor = $type . '_' . $factor;
                $self->{OTPREQUESTS}{$factor}++;
            }
            $self->{OTPREQUESTS}{total}++;
            $self->{OTPSERVERS}{$server}++;
        }
    }

    $self->{DATA}[3]++;
    $self->{USERS}{$user}++;
    $self->{SERVERS}{$server}++;
}

# Write accumulated data out to the RRD file.
sub write_data {
    my ($self, $time) = @_;
    $self->{RRD}->update ($time, @{ $self->{DATA} });
    $self->{DATA} = [ (0) x scalar (@FIELDS) ];
}

# In order to determine the total number of unique users and servers per
# month, we store, in a Berkeley DB database, a count of the number of
# authentications we've seen for a user and for a server per month.
sub end_file {
    my ($self, $time) = @_;
    my $date = strftime ('%Y-%m', localtime $time);
    my %users;
    tie (%users, 'DB_File', "$USERDB.$date.db")
        or die "Cannot tie $USERDB.$date.db: $!\n";
    for my $user (keys %{ $self->{USERS} }) {
        $users{$user} = 0 unless $users{$user};
        $users{$user} += $self->{USERS}{$user};
    }
    untie %users or warn "Cannot flush $USERDB: $!\n";
    $self->{USERS} = {};

    my %otpusers;
    tie (%otpusers, 'DB_File', "$OTPUSERDB.$date.db")
        or die "Cannot tie $OTPUSERDB.$date.db: $!\n";
    for my $user (keys %{ $self->{OTPUSERS} }) {
        $otpusers{$user} = 0 unless $otpusers{$user};
        $otpusers{$user} += $self->{OTPUSERS}{$user};
    }
    untie %otpusers or warn "Cannot flush $OTPUSERDB: $!\n";
    $self->{OTPUSERS} = {};

    my %otpservers;
    tie (%otpservers, 'DB_File', "$OTPSERVERDB.$date.db")
        or die "Cannot tie $OTPSERVERDB.$date.db: $!\n";
    for my $server (keys %{ $self->{OTPSERVERS} }) {
        $otpservers{$server} = 0 unless $otpservers{$server};
        $otpservers{$server} += $self->{OTPSERVERS}{$server};
    }
    untie %otpservers or warn "Cannot flush $OTPSERVERDB: $!\n";
    $self->{OTPSERVERS} = {};

    my %otpfactors;
    tie (%otpfactors, 'DB_File', "$OTPFACTORDB.$date.db")
        or die "Cannot tie $OTPFACTORDB.$date.db: $!\n";
    for my $factor (keys %{ $self->{OTPFACTORS} }) {
        $otpfactors{$factor} = 0 unless $otpfactors{$factor};
        $otpfactors{$factor} += $self->{OTPFACTORS}{$factor};
    }
    untie %otpfactors or warn "Cannot flush $OTPFACTORDB: $!\n";
    $self->{OTPFACTORS} = {};

    my %otprequests;
    tie (%otprequests, 'DB_File', "$OTPREQUESTDB.$date.db")
        or die "Cannot tie $OTPREQUESTDB.$date.db: $!\n";
    for my $factor (keys %{ $self->{OTPREQUESTS} }) {
        $otprequests{$factor} = 0 unless $otprequests{$factor};
        $otprequests{$factor} += $self->{OTPREQUESTS}{$factor};
    }
    untie %otprequests or warn "Cannot flush $OTPREQUESTDB: $!\n";
    $self->{OTPREQUESTS} = {};

    my %servers;
    tie (%servers, 'DB_File', "$SERVERDB.$date.db")
        or die "Cannot tie $SERVERDB.$date.db: $!\n";
    for my $server (keys %{ $self->{SERVERS} }) {
        $servers{$server} = 0 unless $servers{$server};
        $servers{$server} += $self->{SERVERS}{$server};
    }
    untie %servers or warn "Cannot flush $SERVERDB: $!\n";
    $self->{SERVERS} = {};
}

##############################################################################
# Reporting
##############################################################################

# Generate the spec for unique user counts.
sub users_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %users;
    tie (%users, 'DB_File', "$USERDB.$date.db")
        or die "Cannot tie $USERDB.$date.db: $!\n";
    my $spec = {
        name   => 'users',
        values => [ [ string => $date ],
                    [ value  => scalar keys %users ] ]
    };
    untie %users;
    return $spec;
}

# Generate the spec for unique OTP user counts.
sub otpusers_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %otpusers;
    tie (%otpusers, 'DB_File', "$OTPUSERDB.$date.db")
        or die "Cannot tie $OTPUSERDB.$date.db: $!\n";

    my $total_users = scalar keys %otpusers;
    my @otpusers = sort { $otpusers{$b} <=> $otpusers{$a} } keys %otpusers;
    my $spec = {
        name   => 'otpusers',
        values => [ [ string => $date ],
                    [ value  => scalar keys %otpusers ] ],
    };
    untie %otpusers;
    return $spec;
}

# Generate the spec for unique OTP server counts.
sub otpservers_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %servers;
    tie (%servers, 'DB_File', "$OTPSERVERDB.$date.db")
        or die "Cannot tie $OTPSERVERDB.$date.db: $!\n";

    my $total_users = scalar keys %servers;
    my @servers = sort { $servers{$b} <=> $servers{$a} } keys %servers;

    # Since we currently don't have many servers requiring multifactor, we
    # build the table this way to skip any undefined rows.
    my @table;
    for my $row (0..4) {
        last if !defined $servers[$row];
        my @rec = ( [ string => $servers[$row] ],
                    [ string => $servers{$servers[$row]} ]);
        push (@table, \@rec);

    }
    my $spec = {
        name   => 'otpservers',
        values => [ [ string => $date ],
                    [ value  => scalar keys %servers ] ],
        tables => [ { headings => [ 'Server', 'Count' ],
                      data     => \@table,
                    } ]
    };
    untie %servers;
    return $spec;
}

# Generate the spec for OTP factor usage counts.
sub otpfactors_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %otpfactors;
    tie (%otpfactors, 'DB_File', "$OTPFACTORDB.$date.db")
        or die "Cannot tie $OTPFACTORDB.$date.db: $!\n";

    # Find the total number of factors used.
    my $total;
    foreach my $factor (keys %otpfactors) {
        $total += $otpfactors{$factor};
    }

    # Generate the table to be displayed.
    my @table;
    for my $factor ('o1', 'o2', 'o3', 'o4') {
        my $factor_total = $otpfactors{$factor};
        my $percent = sprintf "%.01f%%", $factor_total / $total * 100;
        my @rec = ( [ string => $FACTORS{$factor} ],
                    [ string => $factor_total ],
                    [ string => $percent ] );
        push (@table, \@rec);

    }

    my $spec = {
        name   => 'otpfactors',
        values  => [ [ string => $date ],
                     [ value  => scalar keys %otpfactors ] ],
        tables  => [ { headings => [ 'Factor', 'Count', 'Percent' ],
                       data     => \@table,
                     } ]
    };
    untie %otpfactors;
    return $spec;
}

# Generate the spec for OTP requested factor usage counts.
sub otprequests_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %otprequests;
    tie (%otprequests, 'DB_File', "$OTPREQUESTDB.$date.db")
        or die "Cannot tie $OTPREQUESTDB.$date.db: $!\n";

    my $total= $otprequests{total};

    # Generate the table to be displayed.
    my @table;
    for my $factor (keys %otprequests) {
        next if $factor eq 'total';
        my $factor_total = $otprequests{$factor};
        my $percent = sprintf "%.01f%%", $factor_total / $total * 100;
        my @rec = ( [ string => $REQUESTS{$factor} || $factor ],
                    [ string => $factor_total ],
                    [ string => $percent ] );
        push (@table, \@rec);

    }

    my $spec = {
        name   => 'otprequests',
        values  => [ [ string => $date ],
                     [ value  => scalar keys %otprequests ] ],
        tables  => [ { headings => [ 'Requested Factor', 'Count', 'Percent' ],
                       data     => \@table,
                     } ]
    };
    untie %otprequests;
    return $spec;
}

# Generate the spec for unique server count and the top five servers for the
# past month.
sub servers_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %servers;
    tie (%servers, 'DB_File', "$SERVERDB.$date.db")
        or die "Cannot tie $SERVERDB.$date.db: $!\n";
    my @servers = sort { $servers{$b} <=> $servers{$a} } keys %servers;
    my $spec = {
        name   => 'servers',
        values => [ [ string => $date ],
                    [ value  => scalar keys %servers ] ],
        tables => [ { headings => [ 'Server', 'Count' ],
                      data     => [ [ [ string => $servers[0] ],
                                      [ value  => $servers{$servers[0]} ] ],
                                    [ [ string => $servers[1] ],
                                      [ value  => $servers{$servers[1]} ] ],
                                    [ [ string => $servers[2] ],
                                      [ value  => $servers{$servers[2]} ] ],
                                    [ [ string => $servers[3] ],
                                      [ value  => $servers{$servers[3]} ] ],
                                    [ [ string => $servers[4] ],
                                      [ value  => $servers{$servers[4]} ] ] ],
                    } ]
    };
    untie %servers;
    return $spec;
}

# A basic text report.
sub report_text {
    my ($self, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    $report->add ($self->users_spec ($end));
    $report->add ($self->servers_spec ($end));
    $report->add ($self->otpusers_spec ($end));
    $report->add ($self->otpservers_spec ($end));
    $report->add ($self->otpfactors_spec ($end));
    $report->add ($self->otprequests_spec ($end));
    return $report->text ($template);
}

# Initial pass at a basic HTML report.
sub report_html {
    my ($self, $output, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    $report->add ($self->users_spec ($end));
    $report->add ($self->servers_spec ($end));
    $report->html ($output, $template);
}
