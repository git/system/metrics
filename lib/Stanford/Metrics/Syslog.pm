# Stanford::Metrics::Syslog -- Generic routines for syslog log parsing.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Provides a superclass for syslog-based log parsing, specifically an
# implementation of the module constructor, an implementation of parse_time,
# and an implementation of parse_line that parses a syslog log entry and then
# calls process_line with the resulting information.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Syslog;

require 5.005;

use POSIX qw(mktime);
use Stanford::Metrics::Parse ();

use strict;
use vars qw(@ISA %MONTHS $VERSION);

@ISA = qw(Stanford::Metrics::Parse);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# Used for parsing syslog timestamps.
{
    my $i = 1;
    %MONTHS = map { $_ => $i++ }
        qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
}

##############################################################################
# Implementation
##############################################################################

# We don't do anything fancy or take any parameters.
sub new {
    my $class = shift;
    $class = ref $class || $class;
    return bless ({}, $class);
}

# Given a single line of a log file, the year of the log file, and the month
# of the log file, strip off the leading timestamp and convert it into seconds
# since epoch and then return a list consisting of that timestamp and the rest
# of the line.
#
# A syslog date is in the form:
#
#     mmm DD HH:MM:SS
#
# with no year.  We therefore have to be able to guess at the year by taking
# into account year and month of the file and assuming that if the month of
# the date is greater than the month of the file, the date is actually from
# the previous year.
#
# Warns and returns undef on a failure to parse the date.
#
# Each call to mktime stats /etc/localtime twice and then calls time() on
# Linux, which is painfully slow.  Try to work around this by maintaining a
# simple cache.  If the date is the same as the last date we looked up, return
# the same converted time.
{
    my $lastdate;
    my $lasttime;

    sub parse_time {
        my ($self, $line, $fileyear, $filemonth) = @_;
        $line =~ s/^((\w\w\w) ([ \d]\d) (\d\d):(\d\d):(\d\d)) //
            or return;
        my ($date, $month, $day, $hour, $min, $sec) = ($1, $2, $3, $4, $5, $6);
        if ($lastdate and $date eq $lastdate) {
            return ($lasttime, $line);
        }
        $month = $MONTHS{$month} or return;
        my $year = ($month <= $filemonth) ? $fileyear : $fileyear - 1;
        $year -= 1900;
        $month -= 1;
        my $time = mktime ($sec, $min, $hour, $day, $month, $year);
        $lastdate = $date;
        $lasttime = $time;
        return ($time, $line);
    }
}

# Parse a single line of a syslog log file, given the timestamp and the rest
# of the line without the initial date and time.  Parses out the host,
# program, and PID and discards the host information, and then calls
# process_data with the time stamp, program, PID, and log entry if the line
# could be parsed.
sub parse_data {
    my ($self, $time, $data) = @_;
    my ($program, $pid, $rest)
        = $data =~ /^\S+ ([^:\[\s]+)(?:\[(\d+)\])?:(?: \[ID [^\]]+\])? (.*)/;
    return unless defined $rest;
    if ($rest =~ /^last message repeated (\d+) time/) {
        return unless $$self{LAST};
        for (1..$1) {
            $self->process_data ($time, @{ $$self{LAST} });
        }
    } else {
        $self->process_data ($time, $program, $pid, $rest);
        $$self{LAST} = [ $program, $pid, $rest ];
    }
}

1;
