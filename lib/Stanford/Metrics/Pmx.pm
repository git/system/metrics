# Stanford::Metrics::Pmx -- Metrics reporting for PureMessage logs.
#
# Written by Xueshan Feng <sfeng@stanford.edu>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Parses and reports on PureMessage logs.  Currently, collects the number of
# spam messages, number of viruses, and percentage of these messages over
# the total messages delivered.
#

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Pmx;

require 5.005;

use Stanford::Metrics::Report ();
use Stanford::Metrics::RRD ();
use Stanford::Metrics::Parse ();
use POSIX qw(mktime);

use strict;
use vars qw(@FIELDS @ISA $SPEC $VERSION @SPAM_VIRUS_TABLE);

@ISA = qw(Stanford::Metrics::Parse);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# We don't do anything fancy or take any parameters.
sub new {
    my $class = shift;
    $class = ref $class || $class;
    return bless ({}, $class);
}

@FIELDS = qw(messages spams viruses);

# Table for spam and virus percentage daily average
@SPAM_VIRUS_TABLE = (
    headings => [ 'MsgType', 'Count', 'Percent' ],
    data     => [ [ [ string  => 'Viruses' ],
                    [ average   => 'viruses', 84600 ],
                    [ percent => 'viruses', 'messages' ] ],
                  [ [ string  => 'Spams' ],
                    [ average   => 'spams', 84600 ],
                    [ percent => 'spams', 'messages' ] ] ],
    total   => [ [ string => 'Daily Total Average:' ],
                 [ average  => 'messages',86400 ],
                 [ string => '' ] ]
);
# The specification for the reports.
$SPEC = {
  name     => 'pmx',
  rrd      => [ 'pmx', 'pmx' ],
  sources  => [ @FIELDS ],
  values   => [ [ total    => 'messages' ],
                [ total    => 'spams' ],
                [ total    => 'viruses' ] ],,
  tables => [ { title => 'Viruses and Spam Percent', @SPAM_VIRUS_TABLE} ],
  graphs   => [ { title    => 'Viruses and Spams per day',
                  file     => 'messages',
                  data     => [ 'messages', 'viruses', 'spams' ],
                  commands => [ 'CDEF:virusesSC=viruses,86400,*',
                                'CDEF:spamsSC=spams,86400,*',
                                'CDEF:msgsSC=messages,86400,*',
                                'LINE1:virusesSC#0028D2:Viruses',
                                'LINE1:spamsSC#008800:Spams',
                                'LINE1:msgsSC#CE3E00:Total' ] } ]
};

##############################################################################
# Log parsing
##############################################################################

# The user-visible parse interface.  Takes the root directory and a regex to
# match machines for the logs to parse and an optional path to the root of the
# RRD data.  Creates the RRD object that will store the metrics data and
# initializes the data storage in the object, and then hands things off to
# parse_logs.
sub parse {
    my ($self, $root, $pattern, $rrdroot) = @_;
    my %logs = $self->find_logs ($root, $pattern, 'pmx_log');
    for my $system (sort keys %logs) {
        my $path = "pmx/$system.pmx.rrd";
        $path = "$rrdroot/$path" if defined $rrdroot;
        $$self{RRD} = Stanford::Metrics::RRD->new ($path);
        unless ($$self{RRD}) {
            $$self{RRD} =
                Stanford::Metrics::RRD->create ($path, 'default', @FIELDS);
        }
        $$self{DATA} = [ (0) x 3 ];
        undef $$self{LAST};
        $self->parse_logs (@{ $logs{$system} });
    }
}

# Given a single line of a log file, the year of the log file, and the month
# of the log file, strip off the leading timestamp and convert it into seconds
# since epoch and then return a list consisting of that timestamp and the rest
# of the line.
#
# A pmx_log date is in the form:
#
#     yyyy-mm-ddTHH:MM:SS
#
# line example:
# 2 2005-01-26T12:40:45 [29695:61445,milter] j0QKei4k029694: accepted
#
# Warns and returns undef on a failure to parse the date.
sub parse_time {
    my ($self, $line, $fileyear, $filemonth) = @_;
    $line =~ s/^\d\s+(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)\s+//
        or return;
    my ($year, $month, $day, $hour, $min, $sec) = ($1, $2, $3, $4, $5, $6);
    $month -= 1; # Month begins at 0.
    $year -= 1900;
    my $time = mktime ($sec, $min, $hour, $day, $month, $year);
    return ($time, $line);
}

# Parse a single log line that's already been broken down into timestamp,
# and the rest of data.
sub parse_data {
    my ($self, $time, $data) = @_;
    # Total messages
    if ($data =~ /\s+black:/) {
        $$self{DATA}[1]++;
    } elsif ($data =~ /\s+virus:/) {
        $$self{DATA}[2]++;
    } else {
        # Total messages; include spam (accepted), virus (discarded), aborted.
        $$self{DATA}[0]++;
    }
    $$self{LAST} = [ $data ];
}

# Write accumulated data out to the RRD file.
sub write_data {
    my ($self, $time) = @_;
    $$self{RRD}->update ($time, @{ $$self{DATA} });
    $$self{DATA} = [ (0) x 3 ];
}

# We have no end of file actions.
sub end_file { }

##############################################################################
# Reporting
##############################################################################

# A basic text report.
sub report_text {
    my ($self, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    return $report->text ($template);
}

# Initial pass at a basic HTML report.
sub report_html {
    my ($self, $output, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    $report->html ($output, $template);
}
