# Stanford::Metrics::Sendmail -- Metrics reporting for sendmail logs.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Parses and reports on sendmail logs.  Currently, the only metrics collected
# are number of messages, number of recipients, and byte count of messages
# stored as rates over time as follows:
#
#     Number of messages
#     Number of recipients (each message has at least one)
#     Byte count
#
# We only pay attention to lines like:
#
#     Oct 13 23:55:05 pobox1.Stanford.EDU sendmail[6423]: [ID 801593
#     mail.info] h9E6t4Vr006423: from=<user@example.com>, size=6586, class=0,
#     nrcpts=1, msgid=<82316@example.com>, proto=ESMTP, daemon=MTA,
#     relay=leland7.Stanford.EDU [171.67.16.66]
#
# Each line like this counts as one message, and size and nrcpts are parsed
# out of these lines.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Sendmail;

require 5.005;

use Stanford::Metrics::Report ();
use Stanford::Metrics::RRD ();
use Stanford::Metrics::Syslog ();

use strict;
use vars qw(@FIELDS @ISA $SPEC $VERSION);

@ISA = qw(Stanford::Metrics::Syslog);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The fields stored in the RRD file for sendmail information.
@FIELDS = qw(messages recipients bytes);

# The specification for the reports.
$SPEC = {
  name     => 'sendmail',
  rrd      => [ 'sendmail', 'pobox' ],
  sources  => [ @FIELDS ],
  values   => [ [ total    => 'messages' ],
                [ total    => 'recipients' ],
                [ total    => 'bytes' ] ],
  graphs   => [ { title    => 'Messages per Minute',
                  file     => 'messages',
                  data     => [ 'messages', 'recipients' ],
                  commands => [ 'CDEF:msgsSC=messages,60,*',
                                'CDEF:rcptsSC=recipients,60,*',
                                'LINE1:msgsSC#0028D2:Messages',
                                'LINE1:rcptsSC#CE3E00:Deliveries' ] },
                { title    => 'Kilobytes per Minute',
                  file     => 'bytes',
                  data     => [ 'bytes' ],
                  commands => [ 'CDEF:bytesSC=bytes,60,*,1024,/',
                                'LINE1:bytesSC#0028D2' ] } ]
};

##############################################################################
# Log parsing
##############################################################################

# The user-visible parse interface.  Takes the root directory and a regex to
# match machines for the logs to parse and an optional path to the root of the
# RRD data.  Creates the RRD object that will store the metrics data and
# initializes the data storage in the object, and then hands things off to
# parse_logs.
sub parse {
    my ($self, $root, $pattern, $rrdroot) = @_;
    my %logs = $self->find_logs ($root, $pattern, 'mail');
    for my $system (sort keys %logs) {
        my $path = "sendmail/$system.pobox.rrd";
        $path = "$rrdroot/$path" if defined $rrdroot;
        $$self{RRD} = Stanford::Metrics::RRD->new ($path);
        unless ($$self{RRD}) {
            $$self{RRD} =
                Stanford::Metrics::RRD->create ($path, 'default', @FIELDS);
        }
        $$self{DATA} = [ (0) x 3 ];
        undef $$self{LAST};
        $self->parse_logs (@{ $logs{$system} });
    }
}

# Parse a single log line that's already been broken down into timestamp,
# program, PID, and data for us and just enter that data into the DATA array.
sub process_data {
    my ($self, $time, $program, $pid, $data) = @_;
    my ($size, $nrcpts) =
        ($data =~ /^\S+: from=<[^>]*>.* size=(\d+),.* nrcpts=(\d+),/);
    return unless $nrcpts;
    $$self{DATA}[0]++;
    $$self{DATA}[1] += $nrcpts;
    $$self{DATA}[2] += $size;
}

# Write accumulated data out to the RRD file.
sub write_data {
    my ($self, $time) = @_;
    $$self{RRD}->update ($time, @{ $$self{DATA} });
    $$self{DATA} = [ (0) x 3 ];
}

# We have no end of file actions.
sub end_file { }

##############################################################################
# Reporting
##############################################################################

# A basic text report.
sub report_text {
    my ($self, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    return $report->text ($template);
}

# Initial pass at a basic HTML report.
sub report_html {
    my ($self, $output, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    $report->html ($output, $template);
}
