# Stanford::Metrics::RRDSet -- Wrapper object around sets of RRDs.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# This module wraps a set of Stanford::Metrics::RRD objects into a single
# object, useful for querying information for services that run on multiple
# machines or generating graphs of collective statistics.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::RRDSet;

require 5.005;

use POSIX qw(strftime);
use Stanford::Metrics::RRD ();

use strict;
use vars qw(@GRAPH $ROOT $VERSION);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The standard parameters passed to every graph.
@GRAPH = qw(--lower-limit=0 --interlace --imgformat=PNG);

# The default location of RRD files.
$ROOT = './data';

##############################################################################
# Constructor
##############################################################################

# Open an RRD data set.  A data set is defined by a class and an optional
# type.  The class can either be an absolute path, in which case it names the
# directory that contains all of the RRD files, or a relative path which will
# be taken as relative to $ROOT.  The optional second argument specifies the
# type of database for those services with several databases.
sub new {
    my ($object, $class, $type) = @_;
    die "No RRD class specified" unless defined $class;
    $object = ref $object || $object;
    $class = $ROOT . '/' . $class unless ($class =~ m%^\.?/%);
    opendir (D, $class) or die "Cannot open $class: $!\n";
    my @files = grep { /\.rrd$/ } readdir D;
    closedir D;
    if (defined $type) {
        @files = grep { /\.(?:$type)\.rrd$/ } @files;
    }
    die "No files found for $class" unless @files;
    my @rrds = map { Stanford::Metrics::RRD->new ("$class/$_") } @files;
    my $self = { RRDS => [ @rrds ] };
    return bless ($self, $object);
}

##############################################################################
# Wrapper functions
##############################################################################

# Fetch data from a set of RRDs.  Takes the start and end time (in seconds
# since epoch) and returns a list of data elements, each being the average
# value across that time period, summed across all the RRDs.  Undefined values
# are ignored and not included in the average.  If the calling function
# desires a total, just multiply the returned results by the difference
# between the end and start times.
sub fetch {
    my ($self, $start, $end) = @_;
    my @totals;
    for my $rrd (@{ $$self{RRDS} }) {
        my @data = $rrd->fetch ($start, $end);
        for my $i (0 .. $#data) {
            $totals[$i] += $data[$i];
        }
    }
    return @totals;
}

# Generate a graph from a set of RRDs.  The input is the output file, start
# and end dates, a reference to an array of data values that will be used and
# then an array of other graphing instructions (mostly LINE, AREA, and STACK
# directives).  We return a list consisting of the horizontal and vertical
# size of the graph.
#
# The hard work done by this routine is to set up virtual names for each of
# the data sources in each underlying RRD file and then create a virtual name
# matching the data source name that combines the values from all of the
# separate RRD files into a single virtual data source, where undefined values
# from the underlying RRD files are treated as 0.  The values from the
# separate RRD files are totalled.
sub graph {
    my ($self, $graph, $start, $end, $title, $fields, @options) = @_;
    my @args = ($graph);
    push (@args, '--start', strftime ("%H:%M %Y%m%d", localtime $start));
    push (@args, '--end',   strftime ("%H:%M %Y%m%d", localtime $end));
    push (@args, '--title', $title) if $title;
    push (@args, @GRAPH);
    my $i = 0;
    for my $rrd (@{ $$self{RRDS} }) {
        $i++;
        for my $field (@$fields) {
            push (@args, "DEF:$field$i=" . $rrd->path . ":$field:AVERAGE");
        }
    }
    my $count = $i;
    for my $field (@$fields) {
        my @sources;
        for my $i (1..$count) {
            push (@sources, "$field$i,UN,0,$field$i,IF");
        }
        my $cdef = join (',', @sources) . (',+' x (scalar (@sources) - 1));
        push (@args, "CDEF:$field=$cdef");
    }
    my ($data, $xsize, $ysize) = RRDs::graph (@args, @options);
    my $error = RRDs::error;
    die "Error while generating graph $graph: $error\n" if $error;
    return ($xsize, $ysize);
}
