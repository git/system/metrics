# Stanford::Metrics::RRD -- Wrapper object around RRDs.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# This module wraps RRDs in an object that remembers which RRD file to use and
# provides a somewhat friendlier API for the sorts of things that we need to
# do for metrics collection and reporting.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::RRD;

require 5.005;

use POSIX qw(mktime strftime);
use RRDs ();

use strict;
use vars qw($ROOT $VERSION);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The default location of RRD files.
$ROOT = './data';

##############################################################################
# Constructors and accessors
##############################################################################

# Construct a new object.  Takes the path to the RRD file to use, but doesn't
# open it or do anything with it, just stashes it internally.  If file isn't
# an absolute path, qualify it with the default location.
sub new {
    my ($class, $file) = @_;
    die "No RRD file specified" unless defined $file;
    $class = ref $class || $class;
    $file = $ROOT . '/' . $file unless ($file =~ m%^\.?/%);
    return unless -f $file;
    my $self = { FILE => $file };
    return bless ($self, $class);
}

# Create a new RRD database.  Currently, quite a bit of the details about how
# we do this are hard-coded.  Takes the path of the RRD file to create and a
# list of fields, and creates an RRD file with those fields as ABSOLUTE
# values, a lower limit of 0, and no uppper limit.
#
# There are currently two options on intervals.  The interval "default" has a
# step of five minutes, a heartbeat of one hour, data stored every five
# minutes for a week, data stored every hour for 90 days, and data stored
# every day for five years.  The interval "daily" has a step of one day, a
# heartbeat of one week, and stores data at that interval for five years.
sub create {
    my ($class, $file, $interval, @fields) = @_;
    $class = ref $class || $class;
    $file = $ROOT . '/' . $file unless ($file =~ m%^/%);
    my $year = (localtime)[5] + 1900 - 5;
    if (!$interval || $interval eq 'default') {
        my @data = map { "DS:$_:ABSOLUTE:3600:0:U" } @fields;
        RRDs::create ($file, '--start', "Jan 1 $year", qw/--step 300/, @data,
                      'RRA:AVERAGE:0.5:1:2016',
                      'RRA:AVERAGE:0.5:12:2160',
                      'RRA:AVERAGE:0.5:288:1825');
    } elsif ($interval eq 'daily') {
        my @data = map { "DS:$_:ABSOLUTE:604800:0:U" } @fields;
        RRDs::create ($file, '--start', "Jan 1 $year", qw/--step 86400/,
                      @data, 'RRA:AVERAGE:0.5:1:1825');
    }
    my $error = RRDs::error;
    die "Error while creating $file: $error\n" if $error;
    my $self = { FILE => $file };
    return bless ($self, $class);
}

# Accessor function to return the path of the RRD file, used by RRDSet for
# graphs.
sub path {
    my ($self) = @_;
    return $$self{FILE};
}

##############################################################################
# Wrapper functions
##############################################################################

# Fetch data from an RRD.  Takes the start and end time (in seconds since
# epoch) and returns a list of data elements, each being the average value
# across that time period.  Undefined values are ignored and not included in
# the average.  If the calling function desires a total, just multiply the
# returned results by the difference between the end and start times.
sub fetch {
    my ($self, $start, $end) = @_;
    my @args = ($$self{FILE}, 'AVERAGE', '-s', $start, '-e', $end);
    my ($begin, $step, $names, $data) = RRDs::fetch (@args);
    my $error = RRDs::error;
    die "Error while fetching from $$self{FILE}: $error\n" if $error;
    my @totals;
    for my $line (@$data) {
        for my $i (0 .. $#$line) {
            next unless defined $$line[$i];
            $totals[$i] += $$line[$i];
        }
    }
    for my $i (0 .. $#totals) {
        $totals[$i] /= @$data;
    }
    return @totals;
}

# Update the RRD.  Takes the timestamp and then an array of values.
# Currently, templates are not supported.  Dies on a failure to update except
# for updates out of sequence; for those, since we often deploy test servers
# with some log overlap, we just try to proceed, ignoring the out of order
# data updates that didn't take.
sub update {
    my ($self, $time, @data) = @_;
    my $data = join (':', $time, map { defined ($_) ? $_ : 'U' } @data);
    RRDs::update ($$self{FILE}, $data);
    my $error = RRDs::error;
    return unless $error;
    if ($error =~ /^illegal attempt to update using time .* when last /) {
        warn "Error while updating $$self{FILE} (continuing): $error\n";
    } else {
        die "Error while updating $$self{FILE}: $error\n";
    }
}
