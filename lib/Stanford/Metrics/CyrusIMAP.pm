# Stanford::Metrics::CyrusIMAP -- Metrics reporting for Cyrus IMAP logs.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Parses and reports on Cyrus IMAP logs.  Currently, collects number of
# connections over time broken down by various types of connections and
# methods of authentication, number of daily unique users over time similarly
# broken down, and the last time a user checked their e-mail with each
# different type of connection.
#
# The data stored in the connection RRD files are connection rates as follows:
#
#    Webmail
#    IMAP plaintext over SSL
#    IMAP SASL Kerberos v4
#    IMAP SASL GSSAPI (Kerberos v5)
#    Total IMAP
#    POP plaintext over SSL
#    KPOP
#    POP SASL Kerberos v4
#    POP SASL GSSAPI (Kerberos v5)
#    Total POP
#    Total
#
# Separate RRD files are maintained for daily counts of unique users, with the
# same breakdown between different protocols.  The idea of "uniqueness" is
# reset with each day.  We also store in a separately BerkeleyDB database the
# last time that each user used a given protocol ever.
#
# We only pay attention to the login lines, and any login line that we can't
# parse is printed to stderr for debugging purposes.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::CyrusIMAP;

require 5.005;

use DB_File ();
use File::Copy qw(copy);
use Stanford::Metrics::Report ();
use Stanford::Metrics::RRD ();
use Stanford::Metrics::Syslog ();

use strict;
use vars qw(@AUTH_TABLE @FIELDS @ISA %OFFSET @PROTOCOL_TABLE @SPEC $USERDB
            $VERSION);

@ISA = qw(Stanford::Metrics::Syslog);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The full path to the database that stores the times that all users have last
# checked their mail using various protocols.  Note that this doesn't really
# let us generate historical reports for unique users at various points in the
# past, so will probably have to be rethought at some point.
$USERDB = '/afs/ir/service/metrics/data/cyrus-imap/users.db';

# Offsets into the DATA array for different types of connections.
%OFFSET = (webmail                 => 0,
           'imapd_plain'           => 1,
           'imapd_plain+tls'       => 1,
           'imap_plain+tls'        => 1,
           'imaps_plain+tls'       => 1,
           'imapd_plaintext'       => 1,
           'imapd_plaintext+tls'   => 1,
           'imap_plaintext+tls'    => 1,
           'imaps_plaintext+tls'   => 1,
  	   'imapd_login+tls'       => 1,
           'imap_login+tls'        => 1,
           'imaps_login+tls'        => 1,
           imapd_kerberos_v4       => 2,
           imap_kerberos_v4        => 2,
           'imapd_kerberos_v4+tls' => 2,
           'imaps_kerberos_v4+tls' => 2,
           imapd_gssapi            => 3,
           imap_gssapi             => 3,
           'imapd_gssapi+tls'      => 3,
           'imaps_gssapi+tls'      => 3,
           'imap_gssapi+tls'       => 3,
           imapd                   => 4,
           pop3d_plain             => 5,
           pop3d_login             => 5,
           'pop3s_login+tls'         => 5,
           pop3d_plaintext         => 5,
           'pop3d_plaintext+tls'   => 5,
           'pop3s_plaintext+tls'    => 5,
           'pop3s_plain+tls'       => 5,
           pop3d_kpop              => 6,
           kpop_kpop               => 6,
           kpop_gssapi             => 6,
           pop3d_kerberos_v4       => 7,
           pop3d_gssapi            => 8,
           'pop3s_gssapi+tls'        => 8,
           pop3d                   => 9,
           total                   => 10);

# The fields stored in the RRD file for IMAP connection or unique user
# information.
@FIELDS = qw(webmail imap-plain imap-k4 imap-k5 imap pop-plain pop-kpop
             pop-k4 pop-k5 pop total);

# Table data for a protocol table.
@PROTOCOL_TABLE = (
    headings => [ 'Protocol', 'Count', 'Percent' ],
    data     => [ [ [ string  => 'Webmail' ],
                    [ total   => 'webmail' ],
                    [ percent => 'webmail', 'total' ] ],
                  [ [ string  => 'IMAP' ],
                    [ total   => 'imap' ],
                    [ percent => 'imap', 'total' ] ],
                  [ [ string  => 'POP' ],
                    [ total   => 'pop' ],
                    [ percent => 'pop', 'total' ] ] ],
    total   => [ [ string => 'TOTAL:' ],
                 [ total  => 'total' ],
                 [ string => '' ] ]
);

# Table data for a authentication type table.
@AUTH_TABLE = (
    headings => [ 'Auth Type', 'Count', 'Percent' ],
    data     => [ [ [ string  => 'Webmail' ],
                    [ total   => 'webmail' ],
                    [ percent => 'webmail', 'total' ] ],
                  [ [ string  => 'IMAP SSL' ],
                    [ total   => 'imap-plain' ],
                    [ percent => 'imap-plain', 'total' ] ],
                  [ [ string  => 'IMAP SASL K4' ],
                    [ total   => 'imap-k4' ],
                    [ percent => 'imap-k4', 'total' ] ],
                  [ [ string  => 'IMAP SASL K5' ],
                    [ total   => 'imap-k5' ],
                    [ percent => 'imap-k5', 'total' ] ],
                  [ [ string  => 'POP SSL' ],
                    [ total   => 'pop-plain' ],
                    [ percent => 'pop-plain', 'total' ] ],
                  [ [ string  => 'KPOP' ],
                    [ total   => 'pop-kpop' ],
                    [ percent => 'pop-kpop', 'total' ] ],
                  [ [ string  => 'POP SASL K4' ],
                    [ total   => 'pop-k4' ],
                    [ percent => 'pop-k4', 'total' ] ],
                  [ [ string  => 'POP SASL K5' ],
                    [ total   => 'pop-k5' ],
                    [ percent => 'pop-k5', 'total' ] ] ]
);

# The specification for the reports.
@SPEC = (
{ name     => 'connections',
  rrd      => [ 'cyrus-imap', 'connections' ],
  sources  => [ @FIELDS ],
  values   => [ [ total => 'total' ] ],
  tables   => [ { title    => 'Connections by Protocol',
                  @PROTOCOL_TABLE },
                { title    => 'Connections by Auth Type',
                  @AUTH_TABLE } ],
  graphs   => [ { title    => 'Connections per Minute',
                  file     => 'connections',
                  data     => [ 'total' ],
                  commands => [ 'CDEF:totalSC=total,60,*',
                                'LINE1:totalSC#0028D2' ] },
                { title    => 'Percent Usage by Protocol',
                  file     => 'protocol',
                  data     => [ qw/webmail imap pop total/ ],
                  commands => [ 'CDEF:webmailP=webmail,total,/,100,*',
                                'CDEF:imapP=imap,total,/,100,*',
                                'CDEF:popP=pop,total,/,100,*',
                                'LINE1:webmailP#0028D2:Webmail',
                                'LINE1:popP#CE3E00:POP',
                                'LINE1:imapP#008800:IMAP' ] } ]
},
{ name     => 'users',
  rrd      => [ 'cyrus-imap', 'users' ],
  sources  => [ @FIELDS ],
  graphs   => [ { title    => 'Percent Users by Protocol',
                  file     => 'users',
                  data     => [ qw/webmail imap pop total/ ],
                  commands => [ 'CDEF:webmailP=webmail,total,/,100,*',
                                'CDEF:imapP=imap,total,/,100,*',
                                'CDEF:popP=pop,total,/,100,*',
                                'LINE1:webmailP#0028D2:Webmail',
                                'LINE1:popP#CE3E00:POP',
                                'LINE1:imapP#008800:IMAP' ] } ]
});

##############################################################################
# Log parsing
##############################################################################

# The user-visible parse interface.  Takes the root directory and a regex to
# match machines for the logs to parse and an optional path to the root of the
# RRD data.  Creates the RRD object that will store the metrics data and
# initializes the data storage in the object, and then hands things off to
# parse_logs.
sub parse {
    my ($self, $root, $pattern, $rrdroot) = @_;
    my %logs = $self->find_logs ($root, $pattern, 'imap');
    for my $system (sort keys %logs) {
        my $path = "cyrus-imap/$system.connections.rrd";
        $path = "$rrdroot/$path" if defined $rrdroot;
        $$self{RRD_CXN} = Stanford::Metrics::RRD->new ($path);
        unless ($$self{RRD_CXN}) {
            $$self{RRD_CXN} =
                Stanford::Metrics::RRD->create ($path, 'default', @FIELDS);
        }
        $path = "cyrus-imap/$system.users.rrd";
        $path = "$rrdroot/$path" if defined $rrdroot;
        $$self{RRD_USR} = Stanford::Metrics::RRD->new ($path);
        unless ($$self{RRD_USR}) {
            $$self{RRD_USR} =
                Stanford::Metrics::RRD->create ($path, 'daily', @FIELDS);
        }
        $$self{DATA} = [ (0) x 11 ];
        $$self{UNIQUE} = [ (0) x 11 ];
        $$self{USERS} = {};
        undef $$self{LAST};
        $self->parse_logs (@{ $logs{$system} });
    }
}

# Parse a single log line that's already been broken down into timestamp,
# program, PID, and data for us and just enter that data into the DATA array.
sub process_data {
    my ($self, $time, $program, $pid, $data) = @_;
    # We treat "hostname[ip]", "hostname [ip]", and "[ip]" all as valid
    # system name.
    my ($system, $user, $type) = $data =~ /^login: (.*\s?\[\S+\]) (\S+) (\S+)/;
    return unless $type;
    my ($offset, $class);
    if ($system =~ /^webmail[^.]*\.stanford\.edu/i) {
        $class = $OFFSET{webmail};
    } else {
        my $type = join ('_', $program, lc ($type));
        $class = $OFFSET{$program};
        if ($OFFSET{$type}) {
            $offset = $OFFSET{$type};
        } else {
            warn "unknown authentication type: $program $type\n";
            return;
        }
    }
    $$self{DATA}[$class]++;
    $$self{DATA}[$offset]++ if defined $offset;
    $$self{DATA}[$OFFSET{total}]++;
    if ($$self{USERS}{$user}) {
        my $last = $$self{USERS}{$user}[$offset];
        if ($last == 0) {
            $$self{UNIQUE}[$class]++;
            $$self{UNIQUE}[$offset]++ if defined $offset;
            $$self{UNIQUE}[$OFFSET{total}]++;
        }
    } else {
        $$self{USERS}{$user} = [ (0) x 10 ];
        $$self{UNIQUE}[$class]++;
        $$self{UNIQUE}[$offset]++ if defined $offset;
        $$self{UNIQUE}[$OFFSET{total}]++;
    }
    $$self{USERS}{$user}[$class] = $time;
    $$self{USERS}{$user}[$offset] = $time if defined $offset;
}

# Write accumulated data out to the RRD file.
sub write_data {
    my ($self, $time) = @_;
    $$self{RRD_CXN}->update ($time, @{ $$self{DATA} });
    $$self{DATA} = [ (0) x 11 ];
}

# In order to be able to generate historical reports, we want to save copies
# of the accumulated unique user database at the end of each month.  We do
# this by copying the existing database to a database with the year and month
# appended before updating the database if there is no saved file for the last
# month.
sub save_userdb {
    my ($self, $time) = @_;

    # Get the current date and don't do anything on the first of the month
    # (otherwise, we copy over the database while parsing the last logs of the
    # previous month since they carry over into the next month, and then the
    # copied database isn't actually complete for the previous month).
    my ($day, $month, $year) = (localtime $time)[3..5];
    return if $day == 1;
    $month++;
    $year += 1900;

    # Find the date for last month and create a file name for it.
    if ($month == 1) {
        $month = 12;
        $year--;
    } else {
        $month--;
    }
    $month = sprintf ('%02d', $month);
    my $archive = $USERDB;
    $archive =~ s/\.db$/-$year-$month.db/;

    # If that archive file doesn't already exist, save a copy of the current
    # database under that name.
    if (-f $USERDB && !-f $archive) {
        copy ($USERDB, $archive)
            or warn "Cannot back up user database to $archive: $!\n";
    }
}

# Write out the data that should only be written at the end of a file.  We
# only want to reset the unique user counts at the end of each file, which
# needs to correspond to a daily interval (bad things happen, like
# double-counting, if it doesn't).
sub end_file {
    my ($self, $time) = @_;
    $$self{RRD_USR}->update ($time, @{ $$self{UNIQUE} });
    $$self{UNIQUE} = [ (0) x 11 ];
    $self->save_userdb ($time);
    my %users;
    tie (%users, 'DB_File', $USERDB) or die "Cannot tie $USERDB: $!\n";
    for my $user (keys %{ $$self{USERS} }) {
        my @old;
        my @new = @{ $$self{USERS}{$user} };
        if ($users{$user}) {
            @old = split (';', $users{$user});
        } else {
            @old = (0) x 11;
        }
        my $max = 0;
        for (@new) {
            $max = $_ if $_ > $max;
        }
        unshift (@new, $max);
        for my $i (0..10) {
            $old[$i] = $new[$i] if $new[$i] > $old[$i];
        }
        $users{$user} = join (';', @old);
    }
    $$self{USERS} = {};
    untie %users or warn "Cannot flush $USERDB: $!\n";
}

##############################################################################
# Reporting
##############################################################################

# Find the appropriate user database to use for last mail checked times to
# generate the summary of unique users.  The best that we can do here is to
# use the closest database past the end point of the report; the databases
# will be rotated on a monthly interval.  If the end point is the first of the
# month, use the database from the previous month.
sub find_userdb {
    my ($self, $end) = @_;
    my ($day, $month, $year) = (localtime $end)[3..5];
    $month++ unless $day == 1;
    $year += 1900;
    if ($month == 0) {
        $year--;
        $month = 12;
    }
    $month = sprintf ('%02d', $month);
    my $archive = $USERDB;
    $archive =~ s/\.db$/-$year-$month.db/;
    return (-f $archive ? $archive : $USERDB);
}

# Using the database that stores the last time each user has checked their
# mail using every protocol we recognize, build a spec containing the value of
# total users and a table of usage breakdown by Webmail, IMAP, and POP in
# unique users from the start time of our report to the best end point that we
# can find (it won't be quite right unless the report is being done on a month
# boundary).
sub users_spec {
    my ($self, $start, $end) = @_;
    my $userdb = $self->find_userdb ($end);
    my %users;
    tie (%users, 'DB_File', $userdb) or die "Cannot tie $userdb: $!\n";
    my ($user, $data, $webmail, $pop, $imap, $total);
    while (($user, $data) = each %users) {
        my ($last, @data) = split (';', $data);
        next if ($last < $start);
        $total++;
        if ($data[$OFFSET{webmail}] >= $start) { $webmail++ }
        if ($data[$OFFSET{pop3d}]   >= $start) { $pop++     }
        if ($data[$OFFSET{imapd}]   >= $start) { $imap++    }
    }
    untie %users;
    my $table = [
        [ [ string => 'Webmail' ],
          [ value => $webmail ],
          [ string => sprintf ('%4.1f%%', ($webmail / $total) * 100) ] ],
        [ [ string => 'IMAP' ],
          [ value  => $imap ],
          [ string => sprintf ('%4.1f%%', ($imap / $total) * 100) ] ],
        [ [ string => 'POP' ],
          [ value  => $pop ],
          [ string => sprintf ('%4.1f%%', ($pop / $total) * 100) ] ]
    ];
    my $totals = [ [ string => 'TOTAL:' ], [ value => $total ],
                   [ string => '' ] ];
    my $spec = {
        name   => 'unique',
        values => [ [ value => $total ] ],
        tables => [ { title    => 'Unique Users by Protocol',
                      headings => [ 'Protocol', 'Count', 'Percent' ],
                      data     => $table,
                      total    => $totals } ]
    };
    return $spec;
}

# Initial pass at a basic text report.
sub report_text {
    my ($self, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add (@SPEC);
    $report->add ($self->users_spec ($start, $end));
    return $report->text ($template);
}

# Initial pass at a basic HTML report.
sub report_html {
    my ($self, $output, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add (@SPEC);
    $report->add ($self->users_spec ($start, $end));
    $report->html ($output, $template);
}
