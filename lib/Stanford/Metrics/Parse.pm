# Stanford::Metrics::Parse -- Superclass for log parsing.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010, 2012
#     The Board of Trustees of the Leland Stanford Junior University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# This module provides the superclass for log parsing.  All log parsers should
# inherit from it, which means that each module for a particular log format
# should inherit from it.  It provides the find_logs and parse_logs methods
# that provide the structure of log parsing, and that call into the separate
# functions for each log type.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Parse;

require 5.005;

use File::Find qw(find);

use strict;
use vars qw($INTERVAL %MONTHS $VERSION);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The interval between updates of the RRD databases.  For right now, this is a
# hard-coded value for all logs of five minutes.
$INTERVAL = 300;

##############################################################################
# Generic parsing loop
##############################################################################

# Given a single file path, parse it to find the year, month, and day of that
# log file.  Looks for a directory immediately above the file itself in the
# form %Y-%m-%d.  If no date could be parsed out of the file name, uses the
# last modification time of the file instead (which may be less accurate if
# the file was moved or fiddled with after it was archived).
sub _file_date {
    my ($self, $file) = @_;
    if ($file =~ m%/(\d{4})-(\d{2})-(\d{2})/[^/]+$%) {
        my ($year, $month, $day) = ($1, $2, $3);
        if ($month >= 1 && $month <= 12 && $day >= 1 && $day <= 31) {
            return ($year, $month, $day);
        }
    }

    # Parsing the path failed.  Fall back on stat.
    my ($mtime) = (stat $file)[9];
    return unless $mtime;
    my ($year, $month, $day) = (localtime $mtime)[5,4,3];
    $year += 1900;
    $month++;
    return ($year, $month, $day);
}

# Given two numbers, rounds the first number so that it's a multiple of the
# second and larger than the original number.
sub _round_up {
    my ($self, $num, $base) = @_;
    my $result = int (($num + $base - 1) / $base) * $base;
    $result += $base if ($result == $num);
    return $result;
}

# Given a list of logs, go through those logs a line at a time.  For each
# line, call the parse_line method, passing it the line and the year and month
# of the log file.  For every $INTERVAL seconds of log data, call the
# write_data method.  At the end of each file, call the end_file method.
# Calls die if anything fails.
sub parse_logs {
    my ($self, @logs) = @_;
    for my $log (@logs) {
        my ($year, $month, $day) = $self->_file_date ($log);
        die "Cannot date log $log\n" unless $year;

        # Fork off a zcat process to read the log file.
        my $pid = open (LOG, '-|');
        if (not defined $pid) {
            die "Cannot fork: $!\n";
        } elsif ($pid == 0) {
            my $program = (($log =~ /\.bz2$/) ? 'bzcat' : 'zcat');
            exec ($program, $log) or die "Cannot exec zcat: $!\n";
        }

        # Read the log a line at a time and call parse_line and write_data as
        # appropriate.
        my ($break, $last, $corrupt) = (0, 0, 0);
        local $_;
        while (<LOG>) {
            my ($time, $data) = $self->parse_time ($_, $year, $month);
            unless ($time) {
                warn "Log $log has an unparsable line on line $.\n"
                    unless $corrupt;
                $corrupt = 1;
                next;
            }
            $break = $self->_round_up ($time, $INTERVAL) if ($break == 0);
            while ($time > $break) {
                $self->write_data ($break);
                $break += $INTERVAL;
            }
            $self->parse_data ($time, $data);
            $last = $time;
        }
        $self->write_data ($last) unless ($last == 0);
        $self->end_file ($last) unless ($last == 0);
    }
}

##############################################################################
# Log file locations
##############################################################################

# Given a root directory in which to search, a regex matching the host names
# to accept, and a regex matching the log types to accept, find log files to
# process.  Either .gz or .bz2 extensions are allowed.  The return value is
# somewhat odd, since we want to support maintaining a separate database for
# each log type.  We return an array of pairs of system name and anonymous
# array of logs for that system.  This is suitable for assignment to a hash.
sub find_logs {
    my ($self, $root, $machines, $types) = @_;
    my %files;
    my $wanted = sub {
        return if /^\.\.?$/;
        if (-d $_ && !/^(\d{2}|\d{4}|\d{4}-\d{2}-\d{2})$/) {
            $File::Find::prune = 1;
            return;
        }
        return unless -f $_;
        return unless /^($machines)\.(?:$types)\.(?:gz|bz2)$/;
        my $system = $1;
        $files{$system} ||= [];
        push (@{ $files{$system} }, $File::Find::name);
    };
    $File::Find::dont_use_nlink = 1;
    find ($wanted, $root);
    for (keys %files) {
        $files{$_} = [ sort @{ $files{$_} } ];
    }
    return %files;
}
