# Stanford::Metrics::Apache -- Generic routines for Apache log parsing.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Provides a superclass for Apache-based log parsing, specifically an
# implementation of the module constructor, an implementation of parse_time,
# and an implementation of parse_line that parses an Apache log entry and then
# calls process_line with the resulting information.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Apache;

require 5.005;

use POSIX qw(mktime);
use Stanford::Metrics::Parse ();

use strict;
use vars qw(@ISA %MONTHS $VERSION);

@ISA = qw(Stanford::Metrics::Parse);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# Used for parsing Apache timestamps.
{
    my $i = 1;
    %MONTHS = map { $_ => $i++ }
        qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
}

##############################################################################
# Implementation
##############################################################################

# We don't do anything fancy or take any parameters.
sub new {
    my $class = shift;
    $class = ref $class || $class;
    return bless ({}, $class);
}

# Given a single line of a log file, the year of the log file, and the month
# of the log file, strip off the leading timestamp and convert it into seconds
# since epoch and then return a list consisting of that timestamp and the rest
# of the line.
#
# An Apache date is in the form:
#
#     [ddd mmm DD HH:MM:SS YYYY]
#
# The leading day of the week is ignored.
#
# Warns and returns undef on a failure to parse the date.
#
# Each call to mktime stats /etc/localtime twice and then calls time() on
# Linux, which is painfully slow.  Try to work around this by maintaining a
# simple cache.  If the date is the same as the last date we looked up, return
# the same converted time.
{
    my $lastdate;
    my $lasttime;

    sub parse_time {
        my ($self, $line) = @_;
        $line =~ s/^\[\w\w\w ((\w\w\w) (\d\d) (\d\d):(\d\d):(\d\d) (\d{4}))\] //
            or return;
        my ($date, $month, $day, $hour, $min, $sec, $year)
            = ($1, $2, $3, $4, $5, $6, $7);
        if ($lastdate and $date eq $lastdate) {
            return ($lasttime, $line);
        }
        $month = $MONTHS{$month} or return;
        $year -= 1900;
        $month -= 1;
        my $time = mktime ($sec, $min, $hour, $day, $month, $year);
        $lastdate = $date;
        $lasttime = $time;
        return ($time, $line);
    }
}

1;
