# Stanford::Metrics::Report -- Generate a report from RRD data.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2003, 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# This module contains all the code for generating reports from RRD data.  It
# provides two report generation constructors, text and html, both of which
# take a description of the report as a hash.
#
# A report is defined by a hash like the following:
#
# {
#     name => STRING,
#     rrd => [ STRING, STRING ],
#     sources => [ SOURCE, ... ],
#     values => [ VALUE, ... ],
#     tables => [ TABLE, ... ],
#     graphs => [ GRAPH, ... ],
# }
#
# rrd contains two strings.  The first is the name of the subdirectory of the
# rrdtool database directory where the databases for this report can be found
# and the second is the type of database used by this report (may be undef if
# there is only one type of database in that directory).
#
# sources takes a list of the names of all of the data sources in the RRD set
# that we're generating a report from.  A value is an array like:
#
#    [ STRING, ARG, ARG, ... ]
#
# where the string names the function and the other values are taken as
# arguments to that function.  The function must be one of the following:
#
#    string
#    value
#    average
#    total
#    percent
#
# string just returns its first argument and is used for labels.  value just
# returns its first argument formatted as a number.  average and total take
# the name of a data source in arguments from the RRD set.  Average also takes
# an optional third argument giving the amount to scale the values by (as a
# multiplier).  total finds the total measurement across the entire time span
# of the report.  percent takes the names of two data sources, totals them
# both, and returns the second total over the first total times 100%.
#
# A table is defined as follows:
#
# {
#     title => STRING,
#     headings => [ STRING, ... ],
#     data => [ [ VALUE, ... ], [ VALUE, ... ], ... ],
#     total => [ VALUE, ... ]
# }
#
# A graph is defined as follows:
#
# {
#     title => STRING,
#     file => STRING,
#     data => [ SOURCE, ... ]
#     commands => [ COMMAND, ... ]
# }
#
# data is a list of all of the sources that are needed by the graph.  commands
# is a list of rrdtool graph commands (generally CDEF, AREA, LINE, and STACK).

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Report;

require 5.005;

use Number::Format ();
use Stanford::Metrics::RRDSet ();
use Text::Template ();
use Text::Wrap ();

use strict;
use vars qw(@SPIN $TEMPLATES $VERSION);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The command used to turn web page templates into web pages.
@SPIN = qw(/usr/pubsw/bin/spin -f -o /usr/pubsw/lib/spin-modules/itss.pl);

##############################################################################
# Constructor
##############################################################################

# Create a new report object given an RRDSet, the start and end times, and a
# specification for the report.  This does all the work that's common to both
# text and HTML reports so that the reporting functions can just concentrate
# on the formatting.
sub new {
    my ($class, $start, $end, $rrdroot, $template) = @_;
    $class = ref $class || $class;
    my $self = { START    => $start,
                 END      => $end,
                 RRDROOT  => $rrdroot };
    $self = bless ($self, $class);
    return $self;
}

##############################################################################
# General reporting functions
##############################################################################

# Format a single number.  This mostly just calls into Number::Format, but it
# does some work to figure out the appropriate precision to use.
sub format_number {
    my ($self, $value) = @_;
    my $precision;
    if    ($value > 100) { $precision = 0 }
    elsif ($value > 10)  { $precision = 1 }
    elsif ($value > 1)   { $precision = 2 }
    elsif ($value == 0)  { $precision = 0 }
    else                 { $precision = 3 }
    my $formatter = Number::Format->new;
    return $formatter->format_number ($value, $precision, 1);
}

# Process a single value, returning the result as a string.  Takes the value
# specification (as a pointer to a hash) and a pointer to a data hash.
sub value {
    my ($self, $spec, $data) = @_;
    my ($function, @args) = @$spec;
    my $value;
    if ($function eq 'string') {
        return $args[0];
    } elsif ($function eq 'value') {
        return $self->format_number ($args[0]);
    } elsif ($function eq 'average') {
        $value = $$data{$args[0]};
        $value *= $args[1] if $args[1];
    } elsif ($function eq 'total') {
        $value = $$data{$args[0]} || 0;
        $value *= ($$self{END} - $$self{START});
    } elsif ($function eq 'percent') {
        $value = $$data{$args[0]} || 0;
        my $total = $$data{$args[1]} || 0;
        if ($total == 0) {
            return ' 0.0%';
        } else {
            return sprintf ("%4.1f%%", ($value / $total) * 100);
        }
    } else {
        die "Unknown value function $function\n";
    }
    return $self->format_number ($value);
}

# Given an RRDSet and a list of sources, return a data hash containing the
# totals of those sources over the time interval of the report.
sub data {
    my ($self, $rrdset, @sources) = @_;
    my @data = $rrdset->fetch ($$self{START}, $$self{END});
    my %data;
    for my $i (0 .. $#sources) {
        $data{$sources[$i]} = $data[$i];
    }
    return \%data;
}

# Add a table to the final report.  Takes the specification for the table and
# inserts into the TABLES internal variable the specification with the data
# and totals fields resolved to arrays of strings.
sub add_table {
    my ($self, $spec, $data) = @_;
    my %table;
    $table{title} = $$spec{title} if $$spec{title};
    $table{headings} = [ @{ $$spec{headings} } ];
    my @data = map {
        [ map { $self->value ($_, $data) } @$_ ]
    } @{ $$spec{data} };
    $table{data} = [ @data ];
    if ($$spec{total}) {
        my @totals = map { $self->value ($_, $data) } @{ $$spec{total} };
        $table{total} = [ @totals ];
    }
    $$self{TABLES} ||= [];
    push (@{ $$self{TABLES} }, \%table);
}

# Add some information to the final report.  Takes the specification, which
# should indicate which RRD file to use and then should contain some number of
# values, tables, and graphs.  Values and tables are expanded according to the
# specified data set.  Graphs are just recorded as the pair of an RRDSet and
# the graph specification for later processing.
sub add {
    my ($self, @specs) = @_;
    for my $spec (@specs) {
        my ($data, $rrdset);
        if ($$spec{rrd}) {
            my ($class, $type) = @{ $$spec{rrd} };
            $class = "$$self{RRDROOT}/$class" if $$self{RRDROOT};
            $rrdset = Stanford::Metrics::RRDSet->new ($class, $type);
            $data = $self->data ($rrdset, @{ $$spec{sources} });
        }
        if ($$spec{values}) {
            $$self{VALUES} ||= [];
            my @values = map { $self->value ($_, $data) } @{ $$spec{values} };
            push (@{ $$self{VALUES} }, @values);
        }
        if ($$spec{tables}) {
            $self->add_table ($_, $data) for @{ $$spec{tables} };
        }
        if ($$spec{graphs}) {
            $$self{GRAPHS} ||= [];
            my @graphs = map { [ $rrdset, $_ ] } @{ $$spec{graphs} };
            push (@{ $$self{GRAPHS} }, @graphs);
        }
    }
}

##############################################################################
# Text reports
##############################################################################

# Given a string and a width, return that string padded with leading spaces so
# that it would be centered in that width.
sub text_center {
    my ($self, $text, $width) = @_;
    my $length = length $text;
    return $text if $length >= $width;
    my $padding = ' ' x (($width - $length) / 2);
    return $padding . $text;
}

# Given a list of anonymous arrays, each of which represents a row in a table,
# determine the maximum width of each column of the table and return a list of
# widths.
sub text_widths {
    my ($self, @rows) = @_;
    my @max;
    for my $row (@rows) {
        for my $i (0 .. $#$row) {
            my $length = length $$row[$i];
            $max[$i] = $length if (!$max[$i] || $length > $max[$i]);
        }
    }
    return @max;
}

# Format a text table with a title, headings, one row for each set of values,
# and an optional total at the end.  Widths of columns are chosen based on the
# values that are being formatted.
sub text_table {
    my ($self, $table) = @_;
    my @values = @{ $$table{data} };
    my @totals = @{ $$table{total} } if $$table{total};
    my @headings = ($$table{headings} ? @{ $$table{headings} } : ());
    my @widths = $self->text_widths ([ @headings ], @values, [ @totals ]);
    my $width = 0;
    $width += $_ + 2 for @widths;
    $width -= 2;
    my $format = "%-$widths[0]s";
    $format .= join ('', map { "  %${_}s" } @widths[1 .. $#widths]) . "\n";
    my $output = '';
    if ($$table{title}) {
        $output .= $self->text_center ($$table{title}, $width);
        $output .= "\n\n";
    }
    if ($$table{headings}) {
        $output .= sprintf ($format, @{ $$table{headings} });
        $output .= join ('  ', map { '-' x $_ } @widths) . "\n";
    }
    for my $i (0 .. $#values) {
        $output .= sprintf ($format, @{ $values[$i] });
    }
    if ($$table{total}) {
        $output .= join ('  ', map { '-' x $_ } @widths) . "\n";
        $output .= sprintf ($format, @totals);
    }
    chomp $output;
    return $output;
}

# Wrap a block of text at 74 characters.  Any embedded newlines are changed to
# spaces before wrapping.
sub text_wrap {
    my ($self, $text) = @_;
    $text =~ tr/\n/ /;
    local $Text::Wrap::columns = 74;
    return Text::Wrap::wrap ('', '', $text) . "\n";
}

# Generate a text report and return the resulting text.
sub text {
    my ($self, $template) = @_;
    my @tables;
    if ($$self{TABLES}) {
        @tables = map { $self->text_table ($_) } @{ $$self{TABLES} };
    }
    my $report = Text::Template->new (TYPE => 'FILE', SOURCE => $template);
    my $variables = { table => [ @tables ],
                      value => $$self{VALUES},
                      start => $$self{START},
                      end   => $$self{END} };
    my $result = $report->fill_in (HASH => $variables);
    unless (defined $result) {
        die "Failed to fill in $template: $Text::Template::ERROR\n";
    }
    $result =~ s/^%\s*((?:\S.*\n)+)/$self->text_wrap ($1)/meg;
    return $result;
}

##############################################################################
# HTML reports
##############################################################################

# Generate a graph from the given RRDSet, specification and output directory.
# Returns an array of the x and y size of the graph.
sub html_graph {
    my ($self, $rrdset, $spec, $output) = @_;
    my $file = "$output/$$spec{file}.png";
    return $rrdset->graph ($file, $$self{START}, $$self{END}, $$spec{title},
                           $$spec{data}, @{ $$spec{commands} });
}

# Format an HTML table with headings, one row for each set of values, and an
# optional total at the end.
sub html_table {
    my ($self, $spec) = @_;
    my @values = @{ $$spec{data} };
    my @totals = @{ $$spec{total} } if $$spec{total};
    my @headings = ($$spec{headings} ? @{ $$spec{headings} } : ());
    my $output = "\\table[][\n";
    if ($$spec{headings}) {
        $output .= '  \tablehead';
        $output .= '[' . join ('][', @{ $$spec{headings} }) . ']' . "\n";
    }
    for my $i (0 .. $#values) {
        $output .= '  \tablerow';
        $output .= '[' . join ('][', @{ $values[$i] }) . ']' . "\n";
    }
    if ($$spec{total}) {
        $output .= '  \tablehead';
        $output .= '[' . join ('][', @totals) . ']' . "\n";
    }
    $output .= ']';
    return $output;
}

# Generate an HTML report into the given output directory.
sub html {
    my ($self, $output, $template) = @_;
    my @tables;
    if ($$self{TABLES}) {
        @tables = map { $self->html_table ($_) } @{ $$self{TABLES} };
    }
    for my $graph (@{ $$self{GRAPHS} }) {
        $self->html_graph (@{ $graph }, $output);
    }
    my $report = Text::Template->new (TYPE => 'FILE', SOURCE => $template);
    my $variables = { table => [ @tables ],
                      value => $$self{VALUES},
                      start => $$self{START},
                      end   => $$self{END} };
    my $thread = $report->fill_in (HASH => $variables);
    unless (defined $thread) {
        die "Failed to fill in $template: $Text::Template::ERROR\n";
    }
    my $pid = open (SPIN, '|-');
    if (!defined $pid) {
        die "Cannot fork spin: $!\n";
    } elsif ($pid == 0) {
        open (STDOUT, "> $output/index.html")
            or die "Cannot create output file $output/index.html: $!\n";
        exec @SPIN or die "Cannot exec spin: $!\n";
    } else {
        print SPIN $thread;
    }
    close SPIN;
    if ($? != 0) {
        die "spin exited with status ", ($? >> 8), "\n";
    }
}
