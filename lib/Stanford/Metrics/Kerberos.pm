# Stanford::Metrics::Kerberos -- Metrics reporting for KDC logs.
#
# Written by Russ Allbery <rra@cpan.org>
# Copyright 2010 Board of Trustees, Leland Stanford Jr. University
#
# This program is free software; you may redistribute it and/or modify it
# under the same terms as Perl itself.
#
# Parses and reports on Kerberos KDC logs.  The collected metrics are rates
# over time stored for:
#
#     Initial authentications (user)
#     Initial authentications (CGI)
#     Initial authentications (service principals)
#     Service principal requests (user)
#     Service principal requests (CGI)
#     Service principal requests (service principals)
#
# We only pay attention to lines like:
#
#     Mar 28 23:45:39 kerberos2 krb5kdc[2495]: AS_REQ (1 etypes {1})
#     171.67.16.36: ISSUE: authtime 1269845139, etypes {rep=1 tkt=16 ses=1},
#     service/monitoring@stanford.edu for krbtgt/stanford.edu@stanford.edu
#
#     Mar 28 23:45:41 kerberos2 krb5kdc[2495]: TGS_REQ (7 etypes {16 -133 -128
#     3 1 24 -135}) 171.65.60.21: ISSUE: authtime 1269816115, etypes {rep=1
#     tkt=16 ses=16}, user@stanford.edu for krbtgt/stanford.edu@stanford.edu
#
# The first is an initial authentication and the second is a service principal
# request.  We also look for the Heimdal equivalents:
#
#     Apr 25 23:47:19 kerberos2 kdc[5258]: AS-REQ
#     service/monitoring@stanford.edu from IPv4:171.67.22.24 for
#     krbtgt/stanford.edu@stanford.edu
#     ...
#     Apr 25 23:47:19 kerberos2 kdc[5258]: AS-REQ authtime:
#     2010-04-25T23:47:19 starttime: unset endtime: 2010-04-26T09:47:19 renew
#     till: unset
#
# and:
#
#     Apr 25 23:47:22 kerberos2 kdc[5258]: TGS-REQ user@stanford.edu from
#     IPv4:171.64.170.227 for krbtgt/stanford.edu@stanford.edu [renewable,
#     forwarded, forwardable]
#     ...
#     Apr 25 23:47:22 kerberos2 kdc[5258]: TGS-REQ authtime:
#     2010-04-25T18:36:22 starttime: 2010-04-25T23:47:22 endtime:
#     2010-04-26T19:36:22 renew till: 2010-05-02T18:36:22
#
# Unfortunately, Heimdal spreads this information over multiple lines, so we
# remember the data from the first line but only commit to a count when we see
# the second line.

##############################################################################
# Modules and declarations
##############################################################################

package Stanford::Metrics::Kerberos;

require 5.005;

use DB_File ();
use POSIX qw(strftime);
use Stanford::Metrics::Report ();
use Stanford::Metrics::RRD ();
use Stanford::Metrics::Syslog ();

use strict;
use vars qw(@FIELDS @ISA $SERVERDB $SERVICEDB $SPEC $USERDB $VERSION);

@ISA = qw(Stanford::Metrics::Syslog);

# This version should be increased on any code change to this module.  Always
# use two digits for the minor version with a leading zero if necessary so
# that it will sort properly.
$VERSION = '1.00';

# The fields stored in the RRD file for Kerberos information.
@FIELDS = qw(as-user as-cgi as-service as-total tgs-user tgs-cgi tgs-service
             tgs-total total);

# The paths to the Berkeley DB database used to store unique users, servers,
# and services.  The current year and month will be appended, followed by
# ".db".
$USERDB    = './data/kerberos/users';
$SERVERDB  = './data/kerberos/servers';
$SERVICEDB = './data/kerberos/services';

# The specification for the reports.
$SPEC = {
  name    => 'kerberos',
  rrd     => [ 'kerberos' ],
  sources => [ @FIELDS ],
  values  => [ [ total    => 'as-total'  ],
               [ total    => 'tgs-total' ],
               [ total    => 'total'     ] ],
  tables  => [ { headings => [ 'Type', 'Count', 'Percent' ],
                 data     => [ [ [ string  => 'Users'      ],
                                 [ total   => 'as-user'    ],
                                 [ percent => 'as-user', 'as-total'    ] ],
                               [ [ string  => 'CGI'        ],
                                 [ total   => 'as-cgi'     ],
                                 [ percent => 'as-cgi', 'as-total'     ] ],
                               [ [ string  => 'Services'   ],
                                 [ total   => 'as-service' ],
                                 [ percent => 'as-service', 'as-total' ] ] ],
                 total    => [ [ string => 'TOTAL:'   ],
                               [ total  => 'as-total' ],
                               [ string => ''         ] ] },
               { headings => [ 'Type', 'Count', 'Percent' ],
                 data     => [ [ [ string  => 'Users'       ],
                                 [ total   => 'tgs-user'    ],
                                 [ percent => 'tgs-user', 'tgs-total'    ] ],
                               [ [ string  => 'CGI'         ],
                                 [ total   => 'tgs-cgi'     ],
                                 [ percent => 'tgs-cgi', 'tgs-total'     ] ],
                               [ [ string  => 'Services'    ],
                                 [ total   => 'tgs-service' ],
                                 [ percent => 'tgs-service', 'tgs-total' ] ] ],
                 total    => [ [ string => 'TOTAL:'    ],
                               [ total  => 'tgs-total' ],
                               [ string => ''          ] ] } ],
  graphs  => [ { title    => 'Initial Authentications per Minute',
                 file     => 'initial',
                 data     => [ 'as-user', 'as-cgi', 'as-service' ],
                 commands => [ 'CDEF:userSC=as-user,60,*',
                               'CDEF:cgiSC=as-cgi,60,*',
                               'CDEF:serviceSC=as-service,60,*',
                               'AREA:userSC#0028D2:Users',
                               'AREA:cgiSC#CE3E00:CGI:STACK',
                               'AREA:serviceSC#008800:Services:STACK' ] },
               { title    => 'Service Tickets per Minute',
                 file     => 'service',
                 data     => [ 'tgs-user', 'tgs-cgi', 'tgs-service' ],
                 commands => [ 'CDEF:userSC=tgs-user,60,*',
                               'CDEF:cgiSC=tgs-cgi,60,*',
                               'CDEF:serviceSC=tgs-service,60,*',
                               'AREA:userSC#0028D2:Users',
                               'AREA:cgiSC#CE3E00:CGI:STACK',
                               'AREA:serviceSC#008800:Services:STACK' ] } ]
};

##############################################################################
# Log parsing
##############################################################################

# The user-visible parse interface.  Takes the root directory and a regex to
# match machines for the logs to parse and an optional path to the root of the
# RRD data.  Creates the RRD object that will store the metrics data and
# initializes the data storage in the object, and then hands things off to
# parse_logs.
sub parse {
    my ($self, $root, $pattern, $rrdroot) = @_;
    my %logs = $self->find_logs ($root, $pattern, 'auth');
    for my $system (sort keys %logs) {
        my $path = "kerberos/$system.rrd";
        $path = "$rrdroot/$path" if defined $rrdroot;
        $self->{RRD} = Stanford::Metrics::RRD->new ($path);
        unless ($self->{RRD}) {
            $self->{RRD} =
                Stanford::Metrics::RRD->create ($path, 'default', @FIELDS);
        }
        $self->{DATA} = [ (0) x scalar (@FIELDS) ];
        $self->{USERS} = {};
        $self->{SERVERS} = {};
        undef $self->{LAST};
        delete $self->{SAVED};
        $self->parse_logs (@{ $logs{$system} });
    }
}

# Parse a single log line that's already been broken down into timestamp,
# program, PID, and data for us and just enter that data into the DATA array.
sub process_data {
    my ($self, $time, $program, $pid, $data) = @_;
    my ($type, $user, $principal) =
        ($data =~ /^(AS|TGS)_REQ \([^\)]+\) \S+: ISSUE: .* (\S+) for (\S+)$/);
    unless ($principal) {
        ($type, $user, $principal) =
            ($data =~ /^(AS|TGS)-REQ (\S+) from \S+ for (\S+)(?: \[.*\])?$/);
        if ($principal) {
            $self->{SAVED}{type} = $type;
            $self->{SAVED}{user} = $user;
            $self->{SAVED}{principal} = $principal;
            return;
        }
        if ($data =~ /^(AS|TGS)-REQ authtime: [\d-]+T[\d:]+ /) {
            return unless $self->{SAVED};
            return unless $self->{SAVED}{type} eq $1;
            $type = $self->{SAVED}{type};
            $user = $self->{SAVED}{user};
            $principal = $self->{SAVED}{principal};
            delete $self->{SAVED};
        }
    }
    return unless $principal;
    my $usertype;
    if ($user =~ m,/cgi\@,)                { $usertype = 1 }
    elsif ($user =~ m,/(?:sunet|ipass)\@,) { $usertype = 0 }
    elsif ($user =~ m,/,)                  { $usertype = 2 }
    elsif ($user =~ /\$\@/)                { $usertype = 2 }
    else                                   { $usertype = 0 }
    if ($type eq 'AS') {
        $self->{DATA}[$usertype]++;
        $self->{DATA}[3]++;
        $self->{DATA}[8]++;
    } elsif ($type eq 'TGS') {
        $self->{DATA}[4 + $usertype]++;
        $self->{DATA}[7]++;
        $self->{DATA}[8]++;
    }
    if ($type eq 'AS') {
        $user =~ s/\@.*//;
        if ($usertype == 0) {
            $self->{USERS}{$user}++;
        } else {
            $self->{SERVICES}{$user}++;
        }
    }
    if ($type eq 'TGS') {
        $self->{SERVERS}{$principal}++;
    }
}

# Write accumulated data out to the RRD file.
sub write_data {
    my ($self, $time) = @_;
    $self->{RRD}->update ($time, @{ $self->{DATA} });
    $self->{DATA} = [ (0) x scalar (@FIELDS) ];
}

# In order to determine the total number of unique users per month, we store,
# in a Berkeley DB database, a relationship between year and month and unique
# users.  We do the same thing for servers (target tickets) and services
# (non-user authentications).  We'll let this grow indefinitely, since it's
# small.  Unfortunately, for this to work we need to be processing a full
# month of data at a time, since we can't store partial results.
sub end_file {
    my ($self, $time) = @_;
    my $date = strftime ('%Y-%m', localtime $time);
    my %users;
    tie (%users, 'DB_File', "$USERDB.$date.db")
        or die "Cannot tie $USERDB.$date.db: $!\n";
    for my $user (keys %{ $self->{USERS} }) {
        $users{$user} = 0 unless $users{$user};
        $users{$user} += $self->{USERS}{$user};
    }
    untie %users or warn "Cannot flush $USERDB: $!\n";
    $self->{USERS} = {};
    my %servers;
    tie (%servers, 'DB_File', "$SERVERDB.$date.db")
        or die "Cannot tie $SERVERDB.$date.db: $!\n";
    for my $server (keys %{ $self->{SERVERS} }) {
        $servers{$server} = 0 unless $servers{$server};
        $servers{$server} += $self->{SERVERS}{$server};
    }
    untie %servers or warn "Cannot flush $SERVERDB: $!\n";
    $self->{SERVERS} = {};
    my %services;
    tie (%services, 'DB_File', "$SERVICEDB.$date.db")
        or die "Cannot tie $SERVICEDB.$date.db: $!\n";
    for my $service (keys %{ $self->{SERVICES} }) {
        $services{$service} = 0 unless $services{$service};
        $services{$service} += $self->{SERVICES}{$service};
    }
    untie %services or warn "Cannot flush $SERVICEDB: $!\n";
    $self->{SERVICES} = {};
    delete $self->{SAVED};
}

##############################################################################
# Reporting
##############################################################################

# Generate the spec for unique user count.
sub users_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %users;
    tie (%users, 'DB_File', "$USERDB.$date.db")
        or die "Cannot tie $USERDB.$date.db: $!\n";
    my $spec = {
        name   => 'unique',
        values => [ [ string => $date ],
                    [ value  => scalar keys %users ] ]
    };
    untie %users;
    return $spec;
}

# Generate the spec for unique server count and the top five servers for the
# past month.
sub servers_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %servers;
    tie (%servers, 'DB_File', "$SERVERDB.$date.db")
        or die "Cannot tie $SERVERDB.$date.db: $!\n";
    my @servers = sort { $servers{$b} <=> $servers{$a} } keys %servers;
    my $spec = {
        name   => 'servers',
        values => [ [ string => $date ],
                    [ value  => scalar keys %servers ] ],
        tables => [ { headings => [ 'Service Principal', 'Count' ],
                      data     => [ [ [ string => $servers[0] ],
                                      [ value  => $servers{$servers[0]} ] ],
                                    [ [ string => $servers[1] ],
                                      [ value  => $servers{$servers[1]} ] ],
                                    [ [ string => $servers[2] ],
                                      [ value  => $servers{$servers[2]} ] ],
                                    [ [ string => $servers[3] ],
                                      [ value  => $servers{$servers[3]} ] ],
                                    [ [ string => $servers[4] ],
                                      [ value  => $servers{$servers[4]} ] ] ],
                    } ]
    };
    untie %servers;
    return $spec;
}

# Generate the spec for unique service authentication count.
sub services_spec {
    my ($self, $end) = @_;
    my $date = strftime ('%Y-%m', localtime $end);
    my %services;
    tie (%services, 'DB_File', "$SERVICEDB.$date.db")
        or die "Cannot tie $SERVICEDB.$date.db: $!\n";
    my @services = sort { $services{$b} <=> $services{$a} } keys %services;
    my $spec = {
        name   => 'services',
        values => [ [ string => $date ],
                    [ value  => scalar keys %services ] ]
    };
    untie %services;
    return $spec;
}

# A basic text report.
sub report_text {
    my ($self, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    $report->add ($self->users_spec ($end));
    $report->add ($self->servers_spec ($end));
    $report->add ($self->services_spec ($end));
    return $report->text ($template);
}

# Initial pass at a basic HTML report.
sub report_html {
    my ($self, $output, $start, $end, $template, $rrdroot) = @_;
    my $report = Stanford::Metrics::Report->new ($start, $end, $rrdroot);
    $report->add ($SPEC);
    $report->add ($self->users_spec ($end));
    $report->add ($self->servers_spec ($end));
    $report->add ($self->services_spec ($end));
    $report->html ($output, $template);
}
